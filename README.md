# scrapping-webcam

Scrapping images from a webcam filming a queue on a regular basis to know via a program if there is a queue or not.
Tech used is Airflow and Spark Structured Streaming with Scala.

I used the urllib library to get the image then transformed it into an array using the Tensorflow library.

## First way : Specific pixel coordinates

I used:
* this [Github's project](http://nicodjimenez.github.io/boxLabel/annotate.html) to know the coordinates of 9 specific points.
* the TensorFlow library to process the image into a pandas array.

The final pandas DataFrame will have 1 + 27 columns (1 timestamp + 1 for each pixel color per pixel).

### TODO: 
* Finish the UDFs for each pixel and regarding the period of the day : Morning, Afternoon, Evening.
    * See the 2ndLineTop UDF and complete for other, just need to have the morning value (before 14)
* Finish to define the `pom.xml` to include all the needed dependencies.
* Concatenate the results from the UDFs to define specific rules like:
    * If the tests from 1stLine all return `false` but the rest return `true` then return `Little amount of queue`
    * from 1st Line to mid2nd Line return `false` and the rest is `true` then return `Medium amount of queue`
    * ...
* Define an output file where the final result is appended.

### Further work:
* Use a real Deep Learning algorithm which would train on real queue images then applied to this use case.
* Add a label (Little, Medium, Big) to some images to use a supervised model.
