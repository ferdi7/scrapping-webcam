package com.ferdi.spark.test

import org.apache.spark.sql.types.{BooleanType, DoubleType, IntegerType, StructField, StructType, TimestampType}
import org.scalatest.funspec.AnyFunSpec
import org.apache.spark.sql.{DataFrame, Row}
import java.sql.Timestamp

import com.ferdi.spark.utils.GroundPixelUdf
import com.ferdi.spark.utils.funcTransform
import org.apache.spark.sql.functions.{col, hour}


class PixelCoordsSpec
  extends AnyFunSpec
  with SparkSessionTestWrapper {

  import spark.sqlContext.implicits

  it("transform pixel arrays into boolean based on the colour values of pixel") {

    val schemaPixel = StructType(
      List(
        StructField("time_scrapped", TimestampType, false),
        StructField("3rdLineBottomRed", DoubleType, false),
        StructField("3rdLineBottomGreen", DoubleType, false),
        StructField("3rdLineBottomBlue", DoubleType, false),
        StructField("3rdLineMidRed", DoubleType, false),
        StructField("3rdLineMidGreen", DoubleType, false),
        StructField("3rdLineMidBlue", DoubleType, false),
        StructField("3rdLineTopRed", DoubleType, false),
        StructField("3rdLineTopGreen", DoubleType, false),
        StructField("3rdLineTopBlue", DoubleType, false),
        StructField("2ndLineBottomRed", DoubleType, false),
        StructField("2ndLineBottomGreen", DoubleType, false),
        StructField("2ndLineBottomBlue", DoubleType, false),
        StructField("2ndLineMidRed", DoubleType, false),
        StructField("2ndLineMidGreen", DoubleType, false),
        StructField("2ndLineMidBlue", DoubleType, false),
        StructField("2ndLineTopRed", DoubleType, false),
        StructField("2ndLineTopGreen", DoubleType, false),
        StructField("2ndLineTopBlue", DoubleType, false),
        StructField("1stLineEntranceRed", DoubleType, false),
        StructField("1stLineEntranceGreen", DoubleType, false),
        StructField("1stLineEntranceBlue", DoubleType, false),
        StructField("1stLineMidRed", DoubleType, false),
        StructField("1stLineMidGreen", DoubleType, false),
        StructField("1stLineMidBlue", DoubleType, false),
        StructField("1stLineTopRed", DoubleType, false),
        StructField("1stLineTopGreen", DoubleType, false),
        StructField("1stLineTopBlue", DoubleType, false)
      )
    )


    val values = List(Seq(
      Timestamp.valueOf("2019-12-27 15:51:41.819143"),
      100.0,99.0,104.0,
      89.0,89.0,89.0,
      121.0,119.0,130.0,
      97.0,101.0,100.0,
      124.0,124.0,124.0,
      105.0,104.0,109.0,
      103.0,103.0,103.0,
      87.0,87.0,87.0,
      88.0,88.0,88.0))

    val valuesDF = spark.createDataFrame(spark.sparkContext.parallelize(values.map(e => Row(
      e(0),e(1),e(2),e(3),e(4),e(5),e(6),e(7),e(8),e(9),e(10),e(11),e(12),e(13),e(14),e(15),e(16),
      e(17),e(18),e(19),e(20),e(21),e(22),e(23),e(24),e(25),e(26),e(27)))),
      schemaPixel).withColumn("hour_scrapped", hour(col("time_scrapped")))


    val transformedDF = funcTransform.tempTransform(valuesDF)

    val expectedSchema = StructType(List(
      StructField("time_scrapped", TimestampType, false),
      StructField("hour_scrapped", IntegerType, false),
      StructField("result3rdLineBottom", BooleanType, false),
      StructField("result3rdLineMid", BooleanType, false),
      StructField("result3rdLineTop", BooleanType, false),
      StructField("result2ndLineBottom", BooleanType, false),
      StructField("result2ndLineMid", BooleanType, false),
      StructField("result2ndLineTop", BooleanType, false),
      StructField("result1stLineBottom", BooleanType, false),
      StructField("result1stLineMid", BooleanType, false),
      StructField("result1stLineTop", BooleanType, false)
      )
    )

    val expectedData = List(Seq(
      Timestamp.valueOf("2019-12-27 15:51:41.819143"),
      15,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false
    ))

   val expectedDF = spark.createDataFrame(spark.sparkContext.parallelize(expectedData.map(e => Row(
     e(0),e(1),e(2),e(3),e(4),e(5),e(6),e(7),e(8),e(9), e(10)))), expectedSchema)

  assert(expectedDF.select(col("hour_scrapped")) == transformedDF.select(col("hour_scrapped")))

  }




}
