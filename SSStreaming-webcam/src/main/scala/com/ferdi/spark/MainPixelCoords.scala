package com.ferdi.spark

import com.ferdi.spark.utils.{GroundPixelUdf, funcTransform}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, hour}
import org.apache.spark.sql.types.{DoubleType, StructField, StructType, TimestampType}

object MainPixelCoords extends App {

  override def main(args: Array[String]): Unit = {

    val spark = SparkSession.builder
      .appName("pixelWebcam")
      .master("local[*]")
      .getOrCreate()

    val schemaPixel = StructType(
      List(
        StructField("time_scrapped", TimestampType, false),
        StructField("3rdLineBottomRed", DoubleType, false),
        StructField("3rdLineBottomGreen", DoubleType, false),
        StructField("3rdLineBottomBlue", DoubleType, false),
        StructField("3rdLineMidRed", DoubleType, false),
        StructField("3rdLineMidGreen", DoubleType, false),
        StructField("3rdLineMidBlue", DoubleType, false),
        StructField("3rdLineTopRed", DoubleType, false),
        StructField("3rdLineTopGreen", DoubleType, false),
        StructField("3rdLineTopBlue", DoubleType, false),
        StructField("2ndLineBottomRed", DoubleType, false),
        StructField("2ndLineBottomGreen", DoubleType, false),
        StructField("2ndLineBottomBlue", DoubleType, false),
        StructField("2ndLineMidRed", DoubleType, false),
        StructField("2ndLineMidGreen", DoubleType, false),
        StructField("2ndLineMidBlue", DoubleType, false),
        StructField("2ndLineTopRed", DoubleType, false),
        StructField("2ndLineTopGreen", DoubleType, false),
        StructField("2ndLineTopBlue", DoubleType, false),
        StructField("1stLineEntranceRed", DoubleType, false),
        StructField("1stLineEntranceGreen", DoubleType, false),
        StructField("1stLineEntranceBlue", DoubleType, false),
        StructField("1stLineMidRed", DoubleType, false),
        StructField("1stLineMidGreen", DoubleType, false),
        StructField("1stLineMidBlue", DoubleType, false),
        StructField("1stLineTopRed", DoubleType, false),
        StructField("1stLineTopGreen", DoubleType, false),
        StructField("1stLineTopBlue", DoubleType, false)
      )
    )

    val sdPixelArray = spark.readStream
      .format("csv")
      .schema(schemaPixel)
      .load("/home/nikita/dididi/scrapping-webcam/data/*")
      .withColumn("hour_scrapped", hour(col("time_scrapped")))

    // val eveningOutput = resultDF.select(col("result1stLineTop"), col("result2ndLineBottom")).writeStream.format("console").start().awaitTermination()
    // To stop the stream query
    // eveningOutput.stop()


    val resultDF = funcTransform.tempTransform(sdPixelArray)
    val inferenceOutput = funcTransform.finalTransform(resultDF)

    val finalOutput = inferenceOutput.select(col("time_scrapped"),col("finalResult")).writeStream.format("console").start().awaitTermination()

    // finalOutput.stop()

  }
}
