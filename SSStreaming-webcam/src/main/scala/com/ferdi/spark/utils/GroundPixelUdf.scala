package com.ferdi.spark.utils

import org.apache.spark.sql.functions.udf

object GroundPixelUdf {

  // UDF part

  // 3rd line

  // Morning

  // Afternoon
  // 2019-12-24 14:34:13.950506,
  // 165.0,179.0,179.0,
  // 236.0,232.0,203.0,
  // 251.0,252.0,221.0,

  // Evening
  // 2019-12-24 20:25:29.567069,
  // 76.0,77.0,72.0,
  // 82.0,82.0,82.0,
  // 126.0,127.0,122.0,

  val ground3rdLineBottom = (hour: Integer, s0: Double, s1: Double, s2: Double) => {
    if (hour >= 18) {
      if (((s0 >= 65.0) && (s1 >= 65.0) && (s2 >= 65.0)) && ((s0 <= 85.0) && (s1 <= 85.0) && (s2 <= 85.0))) true
      else false
    } else if (hour >= 13 && hour < 18) {
      if (((s0 >= 155.0) && (s1 >= 169.0) && (s2 >= 169.0)) && ((s0 <= 175.0) && (s1 <= 189.0) && (s2 <= 189.0))) true
      else false
    } else {
      false
    }
  }
  val ground3rdLineBottomUDF = udf(ground3rdLineBottom)

  val ground3rdLineMid = (hour: Integer, s0: Double, s1: Double, s2: Double) => {
    if (hour >= 18) {
      if (((s0 >= 72.0) && (s1 >= 72.0) && (s2 >= 72.0)) && ((s0 <= 92.0) && (s1 <= 92.0) && (s2 <= 92.0))) true
      else false
    } else if (hour >= 13 && hour < 18) {
      if (((s0 >= 226.0) && (s1 >= 222.0) && (s2 >= 193.0) ) && ((s0 <= 246.0) && (s1 <= 242.0) && (s2 <= 213.0))) true
      else false
    }
    else {
      false
    }
  }
  val ground3rdLineMidUDF = udf(ground3rdLineMid)

  val ground3rdLineTop = (hour: Integer, s0: Double, s1: Double, s2: Double) => {
    if (hour >= 18) {
      if (((s0 >= 115.0) && (s1 >= 115.0) && (s2 >= 115.0)) && ((s0 <= 135.0) && (s1 <= 135.0) && (s2 <= 135.0))) true
      else false
    } else if (hour >= 13 && hour < 18) {
      if ( ((s0 >= 241.0) && (s1 >= 242.0) && (s2 >= 211.0)) && ((s0 <= 261.0) && (s1 <= 262.0) && (s2 <= 231.0)) )  true
      else false
    }
    else {
      false
    }
  }
  val ground3rdLineTopUDF = udf(ground3rdLineTop)


  // 2nd line



  // 177.0,182.0,176.0,
  // 214.0,215.0,207.0,
  // 249.0,247.0,208.0,

  // 96.0,97.0,89.0,
  // 119.0,117.0,105.0,
  // 90.0,87.0,80.0,


  val ground2ndLineBottom = (hour: Integer, s0: Double, s1: Double, s2: Double) => {
    if (hour >= 18) {
      if (((s0 >= 90.0) && (s1 >= 90.0) && (s2 >= 80.0) ) && ((s0 <= 110.0) && (s1 <= 110.0) && (s2 <= 105.0) ) ) true
      else false
    } else if (hour >= 13 && hour < 18) {
      if (((s0 >= 167.0) && (s1 >= 172.0) && (s2 >= 166.0) ) && ((s0 <= 187.0) && (s1 <= 192.0) && (s2 <= 186.0))) true
      else false
    }
    else {
      false
    }
  }
  val ground2ndLineBottomUDF = udf(ground2ndLineBottom)

  val ground2ndLineMid = (hour: Integer, s0: Double, s1: Double, s2: Double) => {
    if (hour >= 18) {
      if (((s0 >= 115.0) && (s1 >= 115.0) && (s2 >= 115.0)) && ((s0 <= 135.0) && (s1 <= 135.0) && (s2 <= 135.0))) true
      else false
    } else if (hour >= 13 && hour < 18) {
      if (((s0 >= 204.0) && (s1 >= 205.0) && (s2 >= 207.0)) && ((s0 <= 224.0) && (s1 <= 225.0) && (s2 <= 227.0))) true
      else false
    }
    else {
      false
    }
  }
  val ground2ndLineMidUDF = udf(ground2ndLineMid)

  val ground2ndLineTop = (hour: Integer,s0: Double, s1: Double, s2: Double) => {
    if (hour >= 18){
      if (((s0 >= 85.0) && (s1 >= 85.0) && (s2 >= 83.0)) && ((s0 <= 103.0) && (s1 <= 103.0) && (s2 <= 103.0))) true
      else false
    } else if (hour >= 13 && hour < 18) {
      if (((s0 >= 239.0) && (s1 >= 237.0) && (s2 >= 198.0)) && ((s0 <= 259.0) && (s1 <= 257.0) && (s2 <= 218.0))) true
      else false
    } else { true

    }
  }
  val ground2ndLineTopUDF = udf(ground2ndLineTop)

  // 1st line



  // 217.0,219.0,206.0,
  // 186.0,186.0,162.0,
  // 204.0,203.0,183.0

  // 90.0,91.0,86.0,
  // 78.0,79.0,74.0,
  // 78.0,79.0,74.0

  val ground1stLineBottom = (hour: Integer, s0: Double, s1: Double, s2: Double) => {
    if (hour >= 18) {
      if (((s0 >= 80.0) && (s1 >= 80.0) && (s2 >= 78.0) ) && ((s0 <= 100.0) && (s1 <= 100.0) && (s2 <= 97.0)) ) true
      else false
    } else if (hour >= 13 && hour < 18) {
      if (((s0 >= 207.0) && (s1 >= 209.0) && (s2 >= 196.0) ) && ((s0 <= 227.0) && (s1 <= 229.0) && (s2 <= 216.0))) true
      else false
    }
    else {
      false
    }
  }
  val ground1stLineBottomUDF = udf(ground1stLineBottom)

  val ground1stLineMid = (hour: Integer, s0: Double, s1: Double, s2: Double) => {
    if (hour >= 18) {
      if (((s0 >= 68.0) && (s1 >= 68.0) && (s2 >= 66.0) ) && ((s0 <= 88.0) && (s1 <= 88.0) && (s2 <= 86.0) ) ) true
      else false
    } else if (hour >= 13 && hour < 18) {
      if (((s0 >= 176.0) && (s1 >= 176.0) && (s2 >= 152.0) ) && ((s0 <= 196.0) && (s1 <= 196.0) && (s2 <= 172.0))) true
      else false
    }
    else {
      false
    }
  }
  val ground1stLineMidUDF = udf(ground1stLineMid)

  val ground1stLineTop = (hour: Integer, s0: Double, s1: Double, s2: Double) => {
    if (hour >= 18) {
      if (((s0 >= 70.0) && (s1 >= 69.0) && (s2 >= 64.0) ) && ((s0 <= 90.0) && (s1 <= 90.0) && (s2 <= 84.0) ) ) true
      else false
    } else if (hour >= 13 && hour < 18) {
      if (((s0 >= 194.0) && (s1 >= 193.0) && (s2 >= 173.0) ) && ((s0 <= 214.0) && (s1 <= 213.0) && (s2 <= 193.0))) true
      else false
    }
    else {
      false
    }
  }
  val ground1stLineTopUDF = udf(ground1stLineTop)

  // 2nd part of UDFs to affiliate results to situations

  // If all result1stLine are false and the rest is true Then Little Queue
  // If all result1stLine to result2ndLine are false but the rest is true Then Medium Queue
  // If all is false Then Heavy Queue
  // If all is true Then No Queue

  // r0 is 1stLineEntrance and r8 is 3rdLineTop

  val inferenceResult = (r0: Boolean, r1: Boolean, r2:Boolean,r3: Boolean, r4: Boolean, r5:Boolean,r6: Boolean, r7: Boolean, r8:Boolean) => {
    if (r0 == false && r1 == false && r2 == false && r3 == true && r4 == true && r5 == true && r6 == true && r7 == true && r8 == true) "Little Queue"
    else if (r0 == false && r1 == false && r2 == false && r3 == false && r4 == false && r5 == false && r6 == true && r7 == true && r8 == true) "Medium Queue"
    else if (r0 == false && r1 == false && r2 == false && r3 == false && r4 == false && r5 == false && r6 == false && r7 == false && r8 == false) "Heavy Queue"
    else if (r0 == true && r1 == true && r2 == true && r3 == true && r4 == true && r5 == true && r6 == true && r7 == true && r8 == true) "No Queue"
    else "There is an issue"
  }
  val inferenceResultUDF = udf(inferenceResult)

}
