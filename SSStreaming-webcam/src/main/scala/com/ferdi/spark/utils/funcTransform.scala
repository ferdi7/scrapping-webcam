package com.ferdi.spark.utils

import org.apache.spark.sql.{DataFrame, Dataset, Row}
import com.ferdi.spark.utils.GroundPixelUdf
import org.apache.spark.sql.functions.col

object funcTransform {

  def tempTransform(df: DataFrame): DataFrame = {
    df.withColumn("result3rdLineBottom", GroundPixelUdf.ground3rdLineBottomUDF(col("hour_scrapped"),col("3rdLineBottomRed"),col("3rdLineBottomGreen"),col("3rdLineBottomBlue")))
      .withColumn("result3rdLineMid", GroundPixelUdf.ground3rdLineMidUDF(col("hour_scrapped"),col("3rdLineMidRed"),col("3rdLineMidGreen"),col("3rdLineMidBlue")))
      .withColumn("result3rdLineTop", GroundPixelUdf.ground3rdLineTopUDF(col("hour_scrapped"),col("3rdLineTopRed"),col("3rdLineTopGreen"),col("3rdLineTopBlue")))
      .withColumn("result2ndLineBottom", GroundPixelUdf.ground2ndLineBottomUDF(col("hour_scrapped"),col("2ndLineBottomRed"),col("2ndLineBottomGreen"),col("2ndLineBottomBlue")))
      .withColumn("result2ndLineMid", GroundPixelUdf.ground2ndLineMidUDF(col("hour_scrapped"),col("2ndLineMidRed"),col("2ndLineMidGreen"),col("2ndLineMidBlue")))
      .withColumn("result2ndLineTop", GroundPixelUdf.ground2ndLineTopUDF(col("hour_scrapped"),col("2ndLineTopRed"),col("2ndLineTopGreen"),col("2ndLineTopBlue")))
      .withColumn("result1stLineBottom", GroundPixelUdf.ground1stLineBottomUDF(col("hour_scrapped"),col("1stLineEntranceRed"),col("1stLineEntranceGreen"),col("1stLineEntranceBlue")))
      .withColumn("result1stLineMid", GroundPixelUdf.ground1stLineMidUDF(col("hour_scrapped"),col("1stLineMidRed"),col("1stLineMidGreen"),col("1stLineMidBlue")))
      .withColumn("result1stLineTop", GroundPixelUdf.ground1stLineTopUDF(col("hour_scrapped"),col("1stLineTopRed"),col("1stLineTopGreen"),col("1stLineTopBlue")))
      .drop(col("3rdLineBottomRed")).drop(col("3rdLineBottomGreen")).drop(col("3rdLineBottomBlue"))
      .drop(col("3rdLineMidRed")).drop(col("3rdLineMidGreen")).drop(col("3rdLineMidBlue"))
      .drop(col("3rdLineTopRed")).drop(col("3rdLineTopGreen")).drop(col("3rdLineTopBlue"))
      .drop(col("2ndLineBottomRed")).drop(col("2ndLineBottomGreen")).drop(col("2ndLineBottomBlue"))
      .drop(col("2ndLineMidRed")).drop(col("2ndLineMidGreen")).drop(col("2ndLineMidBlue"))
      .drop(col("2ndLineTopRed")).drop(col("2ndLineTopGreen")).drop(col("2ndLineTopBlue"))
      .drop(col("1stLineEntranceRed")).drop(col("1stLineEntranceGreen")).drop(col("1stLineEntranceBlue"))
      .drop(col("1stLineMidRed")).drop(col("1stLineMidGreen")).drop(col("1stLineMidBlue"))
      .drop(col("1stLineTopRed")).drop(col("1stLineTopGreen")).drop(col("1stLineTopBlue"))
  }

  def finalTransform(df:DataFrame):DataFrame = {
    df.withColumn("finalResult",
      GroundPixelUdf.inferenceResultUDF(
        col("result1stLineBottom"),
        col("result1stLineMid"),
        col("result1stLineTop"),
        col("result2ndLineBottom"),
        col("result2ndLineMid"),
        col("result2ndLineTop"),
        col("result3rdLineBottom"),
        col("result3rdLineMid"),
        col("result3rdLineTop")
      )
    ).drop(col("result1stLineBottom")).drop(col("result1stLineMid")).drop(col("result1stLineTop"))
      .drop(col("result2ndLineBottom")).drop(col("result2ndLineMid")).drop(col("result2ndLineTop"))
      .drop(col("result3rdLineBottom")).drop(col("result3rdLineMid")).drop(col("result3rdLineTop"))
  }

}
