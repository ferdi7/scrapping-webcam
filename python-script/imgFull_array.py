import urllib.request
import time, re, datetime
import pandas as pd
import tensorflow as tf
from google.cloud import storage


def upload_string_to_bucket(blob_name, data, bucket_name):
    """ Upload data to a bucket"""

    # Explicitly use service account credentials by specifying the private key
    # file.
    storage_client = storage.Client.from_service_account_json(
        'XxXxXxXXX.json')

    # print(buckets = list(storage_client.list_buckets())

    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(blob_name)
    blob.upload_from_string(data)


if __name__ == '__main__':

    url = "http://129.102.240.235/extraits/webcam/webcam.jpg"

    scrap_img = urllib.request.urlopen(url)

    load_img = tf.keras.preprocessing.image.load_img(scrap_img)
    img_array = tf.keras.preprocessing.image.img_to_array(load_img)

    final_pixel_arrays = pd.DataFrame(
        {
            'timestamp': datetime.datetime.now(),
            '3rdLineBottomRed': [img_array[429][137][0]],
            '3rdLineBottomGreen': [img_array[429][137][1]],
            '3rdLineBottomBlue': [img_array[429][137][2]],
            '3rdLineMidRed': [img_array[283][163][0]],
            '3rdLineMidGreen': [img_array[283][163][1]],
            '3rdLineMidBlue': [img_array[283][163][2]],
            '3rdLineTopRed': [img_array[192][175][0]],
            '3rdLineTopGreen': [img_array[192][175][1]],
            '3rdLineTopBlue': [img_array[192][175][2]],
            '2ndLineBottomRed': [img_array[369][326][0]],
            '2ndLineBottomGreen': [img_array[369][326][1]],
            '2ndLineBottomBlue': [img_array[369][326][2]],
            '2ndLineMidRed': [img_array[257][286][0]],
            '2ndLineMidGreen': [img_array[257][286][1]],
            '2ndLineMidBlue': [img_array[257][286][2]],
            '2ndLineTopRed': [img_array[258][175][0]],
            '2ndLineTopGreen': [img_array[258][175][1]],
            '2ndLineTopBlue': [img_array[258][175][2]],
            '1stLineEntranceRed': [img_array[362][241][0]],
            '1stLineEntranceGreen': [img_array[362][241][1]],
            '1stLineEntranceBlue': [img_array[362][241][2]],
            '1stLineMidRed': [img_array[312][162][0]],
            '1stLineMidGreen': [img_array[312][162][1]],
            '1stLineMidBlue': [img_array[312][162][2]],
            '1stLineTopRed': [img_array[286][145][0]],
            '1stLineTopGreen': [img_array[286][145][1]],
            '1stLineTopBlue': [img_array[286][145][2]]
        }
    )

    print(final_pixel_arrays)

    # Transform to CSV to save it without header and index is by timestamp
    arrayCsv = final_pixel_arrays.to_csv(header=False, index=None)

    filename = "pixelArray" + str(time.time())
    temp = re.sub("[.]", "", filename)

    final_filename = "/home/nikita/dididi/scrapping-webcam/data/" + temp + ".csv"

    # Save the arrays to a new file in /home/nikita/dididi/scrapping-webcam/data/
    file = open(final_filename, "a+")
    file.write(arrayCsv)
    file.close()

    # Save the file to a Cloud Storage bucket

    # upload_string_to_bucket(temp, arrayCsv, 'data-pixel-arrays')







